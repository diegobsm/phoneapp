package com.applaudostudios.photoapp;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    public static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    /**
     * Function static that return the context of the App Class
     *
     * @return context
     */
    public static Context getContext() {
        return app;
    }
}
