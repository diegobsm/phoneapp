package com.applaudostudios.photoapp.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.applaudostudios.photoapp.R;
import com.applaudostudios.photoapp.fragment.GalleryPhotoFragment;
import com.applaudostudios.photoapp.fragment.ViewerImageFragment;
import com.applaudostudios.photoapp.model.Photo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements GalleryPhotoFragment.OnChangeFragment, ViewerImageFragment.OnActionBarBehavior {

    @BindView(R.id.tbMain)
    Toolbar tbMain;
    private static final String EXT_ARROW_SHOW = "EXT_ARROW_SHOW";
    private boolean mIsArrowBackShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ativity_main);
        ButterKnife.bind(this);
        setSupportActionBar(tbMain);
        if (savedInstanceState != null) {
            mIsArrowBackShow = savedInstanceState.getBoolean(EXT_ARROW_SHOW);
        }
        showHomeIndicator(mIsArrowBackShow);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contPhotoList, new GalleryPhotoFragment())
                .commit();
    }


    /**
     * Add a fragment in contPhotoListFull also send de data to the fragment
     *
     * @param photos   List<Photos> for show in fullScreen
     * @param position of the item clicked
     */
    @Override
    public void onChange(List<Photo> photos, int position) {
        showHomeIndicator(true);
        ViewerImageFragment viewerImageFragment = new ViewerImageFragment();
        Bundle b = new Bundle();
        b.putInt(ViewerImageFragment.EXT_POSITION, position);
        b.putParcelableArrayList(ViewerImageFragment.EXT_PHOTOS, (ArrayList<? extends Parcelable>) photos);
        viewerImageFragment.setArguments(b);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contPhotoListFull, viewerImageFragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Show or hide the actionBar
     * when a fragment notify that must be shown or not
     *
     * @param status for make the condition that hide or show ActionBar
     */
    @Override
    public void onActionBarShowOrHide(boolean status) {
        showAndHideActionBar(status);
    }

    /**
     * Show or hide arrowBack indicator
     *
     * @param status boolean for make condition that if show or hide  the ArrowBack indicator
     */
    public void showHomeIndicator(boolean status) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(status);
            if (status) {
                tbMain.setBackground(getResources().getDrawable(R.drawable.toolbar_transparent));
            } else {
                tbMain.setBackground(getResources().getDrawable(R.drawable.toolbar_normal));
            }
            mIsArrowBackShow = status;
        }
    }

    /**
     * show or hide the actionBar
     *
     * @param status boolean for make condition that if show or hide the actionBar
     */
    public void showAndHideActionBar(boolean status) {
        if (getSupportActionBar() != null) {
            if (status) {
                getSupportActionBar().show();
            } else {
                getSupportActionBar().hide();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            showAndHideActionBar(true);
            showHomeIndicator(false);
        }
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(EXT_ARROW_SHOW, mIsArrowBackShow);
        super.onSaveInstanceState(outState);
    }

}
