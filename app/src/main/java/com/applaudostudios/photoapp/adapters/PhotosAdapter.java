package com.applaudostudios.photoapp.adapters;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.applaudostudios.photoapp.R;
import com.applaudostudios.photoapp.helper.GlideApp;
import com.applaudostudios.photoapp.model.Photo;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder> {

    private OnItemPhotoClick mListener;
    private List<Photo> mData;
    private boolean mIsFullScreen;

    public PhotosAdapter(OnItemPhotoClick onItemPhotoClick, boolean isFullScreen) {
        mListener = onItemPhotoClick;
        mData = new ArrayList<>();
        mIsFullScreen = isFullScreen;
    }


    @Override
    public int getItemViewType(int position) {
        if (mIsFullScreen) {
            return 1;
        } else {
            return super.getItemViewType(position);
        }
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == 1) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo_full_screen, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_photo, viewGroup, false);
        }
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder photoViewHolder, int i) {
        photoViewHolder.bind(mData.get(i));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * Method that set data in the list that contains the data Base
     * and notify to the adapter that is necessary update.
     *
     * @param data
     */
    public void setData(List<Photo> data) {
        mData = data;
        notifyDataSetChanged();
    }


    public class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ctItemPhoto)
        ConstraintLayout mFrameLayout;
        @BindView(R.id.imvPhoto)
        ImageView mImageView;
        @BindView(R.id.pgPhoto)
        ProgressBar pgPhoto;

        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(Photo photo) {
            GlideApp.with(mFrameLayout)
                    .load(photo.getImgSrc())
                    .placeholder(R.drawable.nasa_place_holder)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            pgPhoto.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            pgPhoto.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mImageView);
        }


        @OnClick(R.id.ctItemPhoto)
        public void onClick() {
            mListener.onPhotoClick(getAdapterPosition());
        }

        @OnLongClick(R.id.ctItemPhoto)
        public boolean onLongClick() {
            if (mListener instanceof OnLongClickItemPhoto) {
                ((OnLongClickItemPhoto) mListener).onPhotoLongClick(getAdapterPosition());
            }
            return true;
        }

    }

    /**
     * Interface that handles the callback of the item's click on the list
     */
    public interface OnItemPhotoClick {

        /**
         * Callback that notifies when you click on an item in
         * the list and has the position of that item as a parameter
         *
         * @param position item that was clicked
         */
        void onPhotoClick(int position);
    }

    /**
     * Interface that handles the callback of the item's longClick on the list
     */
    public interface OnLongClickItemPhoto extends OnItemPhotoClick {
        /**
         * Callback that notifies when you longClick on an item in
         * the list and has the position of that item as a parameter
         *
         * @param position item that was longClick
         */
        void onPhotoLongClick(int position);
    }
}
