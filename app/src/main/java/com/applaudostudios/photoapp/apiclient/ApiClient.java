package com.applaudostudios.photoapp.apiclient;

import android.util.Log;

import com.applaudostudios.photoapp.App;
import com.applaudostudios.photoapp.activity.MainActivity;
import com.applaudostudios.photoapp.helper.NetWorkHelper;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/";
    public static final String API_KEY = "HjkiJP2plYpngvVpJuXYBUulN6KvW3Ci4NOIZrAP";
    private static final String CACHE_CONTROL = "Cache-Control";
    private static final String TAG = MainActivity.class.getName();

    private static ApiClient mInstance = null;
    private PhotoService mPhotoService;

    public ApiClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new cacheInterceptor())
                .addInterceptor(new OfflineInterceptor())
                .cache(cache())
                .build();

        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        mPhotoService = mRetrofit.create(PhotoService.class);
    }

    /**
     * Class that implements the Interceptor class to intercept request
     * which are done in a minute so as not to make requests for the rest.
     */
    public static class cacheInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .header(CACHE_CONTROL, "public, max-age=" + 60)
                    .build();
        }
    }

    /**
     * Class that implements the Interceptor class to intercept requests
     * when there is no internet and that responds with the data it has in cache.
     */
    public static class OfflineInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (!NetWorkHelper.isNetworkAvailable(App.getContext())) {
                request = request.newBuilder()
                        .header(CACHE_CONTROL,
                                "public, only-if-cached, max-stale=" + 2419200)
                        .build();
            }
            return chain.proceed(request);
        }
    }

    /**
     * This method creates an instance of the cache class with 10 MB of storage,
     * to insert it in the client of type OKHTTP
     *
     * @return Cache instance
     */
    private Cache cache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(App.getContext().getCacheDir(), "apiResponses"),
                    10 * 1024 * 1024);
        } catch (Exception e) {
            Log.e(TAG, "Could not create Cache!");
        }
        return cache;
    }

    /**
     * Method static that return a unique Object of type ApiClient
     *
     * @return ApiClient Instance
     */
    public static ApiClient getInstance() {
        if (mInstance == null) {
            mInstance = new ApiClient();
        }
        return mInstance;
    }

    /**
     * This function return ua object of type PhotoService
     *
     * @return PhotoService instance
     */
    public PhotoService getPhotoService() {
        return mPhotoService;
    }

}
