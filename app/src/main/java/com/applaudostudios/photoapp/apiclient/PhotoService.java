package com.applaudostudios.photoapp.apiclient;

import com.applaudostudios.photoapp.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PhotoService {

    @GET("photos")
    Call<Response<List<Photo>>> getPhotos(@Query("sol") int sol,
                                          @Query("page") int page,
                                          @Query("api_key") String apiKey);
}
