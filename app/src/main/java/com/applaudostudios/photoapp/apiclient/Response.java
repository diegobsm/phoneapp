package com.applaudostudios.photoapp.apiclient;

import com.google.gson.annotations.SerializedName;

public class Response<T> {
    @SerializedName("photos")
    private T mPhoto;

    public T getPhoto() {
        return mPhoto;
    }

    public void setPhoto(T mPhoto) {
        this.mPhoto = mPhoto;
    }
}
