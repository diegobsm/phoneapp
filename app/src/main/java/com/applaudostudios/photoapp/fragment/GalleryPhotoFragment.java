package com.applaudostudios.photoapp.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.applaudostudios.photoapp.R;
import com.applaudostudios.photoapp.adapters.PhotosAdapter;
import com.applaudostudios.photoapp.apiclient.ApiClient;
import com.applaudostudios.photoapp.apiclient.PhotoService;
import com.applaudostudios.photoapp.apiclient.Response;
import com.applaudostudios.photoapp.model.Photo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryPhotoFragment extends Fragment implements PhotosAdapter.OnItemPhotoClick {

    @BindView(R.id.rcvPhotos)
    RecyclerView rcvPhotos;
    @BindView(R.id.pgPagination)
    ProgressBar pgPagination;
    private OnChangeFragment mListener;
    private List<Photo> mPhotos;
    private int mNumPage = 1;

    private PhotosAdapter mPhotoAdapter;

    public GalleryPhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnChangeFragment) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mListener = (OnChangeFragment) activity;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_galery_photo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        makeRequest(mNumPage, false);
    }

    /**
     * init all views of in this Fragment
     */
    private void initView() {
        mPhotoAdapter = new PhotosAdapter(this, false);
        rcvPhotos.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rcvPhotos.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    pgPagination.setVisibility(View.VISIBLE);
                    makeRequest(mNumPage, true);
                }
            }
        });
        rcvPhotos.setAdapter(mPhotoAdapter);
    }

    /**
     * This function takes two parameters one the page to show and the other
     * if it is doing pagination or not, makes a request to the API with Retrofit
     * and inserts the data in the Adapter so that renders
     *
     * @param page         number of the page to show
     * @param isPagination Boolean to identify if it is a paging request or not
     */
    private void makeRequest(int page, final boolean isPagination) {
        ApiClient apiClient = ApiClient.getInstance();
        PhotoService photoService = apiClient.getPhotoService();
        Call<Response<List<Photo>>> call = photoService.getPhotos(50, page, ApiClient.API_KEY);
        call.enqueue(new Callback<Response<List<Photo>>>() {
            @Override
            public void onResponse(Call<Response<List<Photo>>> call, retrofit2.Response<Response<List<Photo>>> response) {
                if (response.isSuccessful()) {
                    Response<List<Photo>> listResponse = response.body();
                    if (listResponse != null) {
                        if (isPagination) {
                            mPhotos.addAll(listResponse.getPhoto());
                            pgPagination.setVisibility(View.GONE);
                        } else {
                            mPhotos = listResponse.getPhoto();
                        }
                        mPhotoAdapter.setData(mPhotos);
                        mNumPage++;
                    }
                } else {
                    pgPagination.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Response<List<Photo>>> call, Throwable t) {
                Log.d("TAG_ERROR", t.getMessage());
                pgPagination.setVisibility(View.GONE);
            }
        });
    }


    /**
     * Callback that is executed when the click event of an item in the list is activated,
     * notify to the Activity that an item has been clicked
     *
     * @param position of the item clicked
     */
    @Override
    public void onPhotoClick(int position) {
        mListener.onChange(mPhotos, position);
    }

    /**
     * This interface is used to change add notify the class,
     * that implements it to change add a fragment
     */
    public interface OnChangeFragment {
        /**
         * @param photos   List<Photos> for show in fullScreen
         * @param position of the item clicked
         */
        void onChange(List<Photo> photos, int position);
    }
}
