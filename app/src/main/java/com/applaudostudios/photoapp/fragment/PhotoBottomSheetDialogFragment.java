package com.applaudostudios.photoapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.applaudostudios.photoapp.R;
import com.applaudostudios.photoapp.model.Photo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private static final String EXT_PHOTO = "EXT_PHOTO";
    private static Photo mPhoto;
    @BindView(R.id.txvNameCamera)
    TextView txvNameCamera;
    @BindView(R.id.txvLaunch)
    TextView txvLaunch;
    @BindView(R.id.txvLand)
    TextView txvLand;
    @BindView(R.id.btnShare)
    Button btnShare;


    /**
     * Method static that return a instance of the BottomSheetDialogFragment
     *
     * @param photo Object of type Photo for fill the textViews
     * @return BottomSheetDialogFragment instance
     */
    public static PhotoBottomSheetDialogFragment newInstance(Photo photo) {
        PhotoBottomSheetDialogFragment fragment = new PhotoBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXT_PHOTO, photo);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buttom_sheet_photo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            mPhoto = getArguments().getParcelable(EXT_PHOTO);
            setData();
        }
    }

    /**
     * Insert the data in the TextViews
     */
    public void setData() {
        txvNameCamera.setText(mPhoto.getCamera().getName());
        StringBuilder launch = new StringBuilder();
        StringBuilder land = new StringBuilder();
        launch.append(txvLaunch.getText().toString()).append(mPhoto.getRover().getLaunchDate());
        land.append(txvLand.getText().toString()).append(mPhoto.getRover().getLandingDate());
        txvLaunch.setText(launch.toString());
        txvLand.setText(land.toString());
    }

    /**
     * Method that is executed when the click event of the bottom is activated.
     * Make a intent implicit to ACTION_SEND for share url of the image.
     */
    @OnClick(R.id.btnShare)
    public void onClick() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra("URL", mPhoto.getImgSrc());
        intent.setType("text/plain");
        startActivity(intent);
    }
}
