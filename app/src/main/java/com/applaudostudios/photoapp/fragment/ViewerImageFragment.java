package com.applaudostudios.photoapp.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applaudostudios.photoapp.R;
import com.applaudostudios.photoapp.adapters.PhotosAdapter;
import com.applaudostudios.photoapp.model.Photo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewerImageFragment extends Fragment implements PhotosAdapter.OnLongClickItemPhoto {

    public static final String EXT_POSITION = "EXT_POSITION";
    public static final String EXT_PHOTOS = "EXT_PHOTOS";

    private List<Photo> mPhotos;
    private int mPosition;
    private OnActionBarBehavior mListener;
    private boolean mIsActionBarShow = false;

    @BindView(R.id.ctMainViewer)
    CoordinatorLayout ctMainViewer;
    @BindView(R.id.rcvPhotoFull)
    RecyclerView rcvPhotoFull;

    public ViewerImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnActionBarBehavior) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mListener = (OnActionBarBehavior) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPosition = getArguments().getInt(EXT_POSITION);
            mPhotos = getArguments().getParcelableArrayList(EXT_PHOTOS);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_viewer_image, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    /**
     * init all views of in this Fragment
     */
    public void initView() {
        rcvPhotoFull.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        PhotosAdapter mPhotosAdapter = new PhotosAdapter(this, true);
        rcvPhotoFull.setAdapter(mPhotosAdapter);
        mPhotosAdapter.setData(mPhotos);
        rcvPhotoFull.scrollToPosition(mPosition);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rcvPhotoFull);
        mListener.onActionBarShowOrHide(false);
    }


    /**
     * Callback that is executed when the click event of an item in the list is activated,
     * notify the activity that has been clicked to show or hide the actionBar
     *
     * @param position of the item in the list
     */
    @Override
    public void onPhotoClick(int position) {
        if (!mIsActionBarShow) {
            mListener.onActionBarShowOrHide(true);
            mIsActionBarShow = true;
        } else {
            mListener.onActionBarShowOrHide(false);
            mIsActionBarShow = false;
        }
    }

    /**
     * Callback that is executed when the longClick event of an item in the list is activated,
     * also creates an instance of a fragment bottomSheet and shows it
     *
     * @param position position of the item of the list
     */
    @Override
    public void onPhotoLongClick(int position) {
        if (getFragmentManager() != null) {
            PhotoBottomSheetDialogFragment mBottomSheet = PhotoBottomSheetDialogFragment.newInstance(mPhotos.get(position));
            mBottomSheet.show(getFragmentManager(), "TAG_BOTTOM_SHEET");
        }
    }

    /**
     * This interface is used as a callback to change the visibility state
     * of the action bar of the Activity that contains the fragment
     */
    public interface OnActionBarBehavior {
        /**
         * Callback that if execute in the Class that implement this interface
         * has a boolean type parameter
         *
         * @param status for make the condition that hide or show ActionBar
         */
        void onActionBarShowOrHide(boolean status);
    }
}
