package com.applaudostudios.photoapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Photo implements Parcelable {

    @SerializedName("id")
    private int mId;
    @SerializedName("camera")
    private Camera mCamera;
    @SerializedName("img_src")
    private String mImgSrc;
    @SerializedName("rover")
    private Rover mRover;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public Camera getCamera() {
        return mCamera;
    }

    public void setCamera(Camera mCamera) {
        this.mCamera = mCamera;
    }

    public String getImgSrc() {
        return mImgSrc;
    }

    public void setImgSrc(String mImgSrc) {
        this.mImgSrc = mImgSrc;
    }

    public Rover getRover() {
        return mRover;
    }

    public void setRover(Rover mRover) {
        this.mRover = mRover;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeParcelable(this.mCamera, flags);
        dest.writeString(this.mImgSrc);
        dest.writeParcelable(this.mRover, flags);
    }

    public Photo() {
    }

    protected Photo(Parcel in) {
        this.mId = in.readInt();
        this.mCamera = in.readParcelable(Camera.class.getClassLoader());
        this.mImgSrc = in.readString();
        this.mRover = in.readParcelable(Rover.class.getClassLoader());
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
