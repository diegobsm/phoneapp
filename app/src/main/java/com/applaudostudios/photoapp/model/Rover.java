package com.applaudostudios.photoapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Rover implements Parcelable {

    @SerializedName("landing_date")
    private Date mLandingDate;
    @SerializedName("launch_date")
    private Date mLaunchDate;

    public Date getLandingDate() {
        return mLandingDate;
    }

    public void setLandingDate(Date mLandingDate) {
        this.mLandingDate = mLandingDate;
    }

    public Date getLaunchDate() {
        return mLaunchDate;
    }

    public void setLaunchDate(Date mLaunchDate) {
        this.mLaunchDate = mLaunchDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mLandingDate != null ? this.mLandingDate.getTime() : -1);
        dest.writeLong(this.mLaunchDate != null ? this.mLaunchDate.getTime() : -1);
    }

    public Rover() {
    }

    protected Rover(Parcel in) {
        long tmpMLandingDate = in.readLong();
        this.mLandingDate = tmpMLandingDate == -1 ? null : new Date(tmpMLandingDate);
        long tmpMLaunchDate = in.readLong();
        this.mLaunchDate = tmpMLaunchDate == -1 ? null : new Date(tmpMLaunchDate);
    }

    public static final Parcelable.Creator<Rover> CREATOR = new Parcelable.Creator<Rover>() {
        @Override
        public Rover createFromParcel(Parcel source) {
            return new Rover(source);
        }

        @Override
        public Rover[] newArray(int size) {
            return new Rover[size];
        }
    };
}
